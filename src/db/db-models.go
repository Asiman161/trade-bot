package db

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Key    string `json:"key"`
	Secret string `json:"secret"`
	UserId uint   `gorm:"unique_index;not_null" json:"user_id"`
	Trade  []Trade
}

// Price - it's price when we want to enter to market
type Trade struct {
	gorm.Model
	Symbol        string        `gorm:"not null" json:"symbol" validate:"min=5,max=10"`
	Side          string        `gorm:"not null" json:"side" validate:"regexp=^(BUY|SELL)$"`
	LookBy        string        `gorm:"not null" json:"look_by" validate:"regexp=^(BID|ASK|CURRENT)$"`
	TakePosition  float64       `gorm:"not null" json:"take_position,string" validate:"nonzero"`
	StopPosition  float64       `gorm:"not null" json:"stop_position,string" validate:"nonzero"`
	Price         float64       `gorm:"not null" json:"price,string" validate:"nonzero"`
	Quantity      float64       `gorm:"not null" json:"quantity,string" validate:"nonzero"`
	BuyDirection  string        `gorm:"not null" json:"buy_direction" validate:"regexp=^(DOWN|UP)$"`
	Type          string        `gorm:"not null" json:"type" validate:"regexp=^(LIMIT|MARKET)$"`
	CloseByStop   bool          `json:"close_by_stop"`
	IsActive      bool          `json:"is_active"`
	IsDone        bool          `json:"is_done"`
	IsCancel      bool          `json:"is_cancel"`
	PlacedOrders  []PlacedOrder `json:"placed_orders"`
	TradeDetail   TradeDetail   `json:"trade_detail"`
	UserID        uint          `json:"user_id" validate:"nonzero"`
	TradeDetailID uint          `json:"trade_detail_id"`
}

type TradeDetail struct {
	gorm.Model
	TradeId         uint    `json:"trade_id"`
	BuyPrice        float64 `json:"buy_price"`
	SellPrice       float64 `json:"sell_price"`
	BuyExecutedQty  float64 `json:"buy_executed_qty"`
	SellExecutedQty float64 `json:"sell_executed_qty"`
}

type ErrorsLog struct {
	gorm.Model
	Text string
}

type PlacedOrder struct {
	gorm.Model
	Symbol        string
	OrderId       int64
	ClientOrderId string
	TransactTime  int64
	Side          string
}
