package store

import (
	"sync"
	"errors"
	"trade-bot/src/db"
	"fmt"
	"trade-bot/src/util"
)

type BinanceSocketData struct {
	EventType                   string `json:"e"`
	EventTime                   int64  `json:"E"`
	Symbol                      string `json:"s"`
	PriceChange                 string `json:"p"`
	PriceChangePercent          string `json:"P"`
	WeightedAveragePrice        string `json:"w"`
	PreviousDayClosePrice       string `json:"x"`
	CurrentDayClosePrice        string `json:"c"`
	CloseTradesQuantity         string `json:"Q"`
	BestBidPrice                string `json:"b"`
	BestBIdQuantity             string `json:"B"`
	BestAskPrice                string `json:"a"`
	BestAskQuantity             string `json:"A"`
	OpenPrice                   string `json:"o"`
	HighPrice                   string `json:"h"`
	LowPrice                    string `json:"l"`
	TotalTradedBaseAssetVolume  string `json:"v"`
	TotalTradedQuoteAssetVolume string `json:"q"`
	StatisticsOpenTime          int64  `json:"O"`
	StatisticsCloseTime         int64  `json:"C"`
	FirstTradeId                int64  `json:"F"`
	LastTradeId                 int64  `json:"L"`
	TotalNumberOfTrades         int64  `json:"n"`
}

type SafeRealTimeBinanceStorage struct {
	realTimeBinanceStorage map[string]BinanceSocketData
	sync.RWMutex
}

var realTimeBinanceStorage *SafeRealTimeBinanceStorage

// this's triple map.
// it contains : Store[Symbol][UserId][TradeId]
// @example: Store["BTCUSDT"]["UserId421"]["randomId12"]{...}
type TradePath map[string]map[uint]map[uint]db.Trade

type WaitingTrades struct {
	Store TradePath
	sync.RWMutex
}

type ActiveTrades struct {
	Store TradePath
	sync.RWMutex
}

type ClosingTrades struct {
	Store TradePath
	sync.RWMutex
}

var users = make(map[uint]db.User)

// Interface for manipulating with stores
// You can use parameter "safe" if you need to RLock store
type TradesActions interface {
	AddTrade(trade db.Trade, safe ...bool)
	RemoveTrade(trade db.Trade, safe ...bool)
	GetTrade(symbol string, userId, tradeId uint, safe ...bool) (db.Trade, error)
	GetStore() *TradePath
	UpdateTradeInfo(trade db.Trade)
}

var waitingTradesStore *WaitingTrades
var activeTradesStore *ActiveTrades
var closingTradesStore *ClosingTrades

//Start of Binance real time storage.
//It contains 1 tick data only and updates every second
func (srtbs *SafeRealTimeBinanceStorage) UpdateData(data []BinanceSocketData) {
	srtbs.RLock()
	for _, element := range data {
		srtbs.realTimeBinanceStorage[element.Symbol] = element
	}
	srtbs.RUnlock()
}

func (srtbs *SafeRealTimeBinanceStorage) GetDataMap() map[string]BinanceSocketData {
	return srtbs.realTimeBinanceStorage
}

func (srtbs *SafeRealTimeBinanceStorage) GetData(key string) BinanceSocketData {
	return srtbs.realTimeBinanceStorage[key]
}

func GetStorage() *SafeRealTimeBinanceStorage {
	if realTimeBinanceStorage == nil {
		realTimeBinanceStorage = &SafeRealTimeBinanceStorage{realTimeBinanceStorage: make(map[string]BinanceSocketData)}
	}
	return realTimeBinanceStorage
}

//End of Binance real time storage.

func GetWaitingTradesStore() *WaitingTrades {
	if waitingTradesStore == nil {
		waitingTradesStore = &WaitingTrades{}
		waitingTradesStore.Store = make(TradePath)
	}
	return waitingTradesStore
}

func GetActiveTradesStore() *ActiveTrades {
	if activeTradesStore == nil {
		activeTradesStore = &ActiveTrades{}
		activeTradesStore.Store = make(TradePath)
	}
	return activeTradesStore
}

func GetClosingTradesStore() *ClosingTrades {
	if closingTradesStore == nil {
		closingTradesStore = &ClosingTrades{}
		closingTradesStore.Store = make(TradePath)
	}
	return closingTradesStore
}

func (wt *WaitingTrades) MoveTradeToActiveTrades(trade db.Trade) {
	waitingTradesStore.RLock()
	activeTradesStore.RLock()
	trade.IsActive = false
	trade.Side = "SELL"
	AddTrade(activeTradesStore, trade)
	RemoveTrade(waitingTradesStore, trade)
	waitingTradesStore.RUnlock()
	activeTradesStore.RUnlock()
}

func (wt *WaitingTrades) MoveTradeToClosingTrades(trade db.Trade) {
	waitingTradesStore.RLock()
	closingTradesStore.RLock()
	trade.IsActive = false
	trade.Side = "SELL"
	AddTrade(closingTradesStore, trade)
	RemoveTrade(waitingTradesStore, trade)
	waitingTradesStore.RUnlock()
	closingTradesStore.RUnlock()
}

// Interface implementing
/**
 * safe - lock goroutines to prevent receiving old or wrong data
 */
func AddTrade(store TradesActions, trade db.Trade, safe ...bool) {
	store.AddTrade(trade, safe...)
}

func RemoveTrade(store TradesActions, trade db.Trade, safe ...bool) {
	store.RemoveTrade(trade, safe...)
}

func GetTrade(store TradesActions, symbol string, userId uint, tradeId uint, safe ...bool) (db.Trade, error) {
	return store.GetTrade(symbol, userId, tradeId, safe...)
}

func GetStore(store TradesActions) *TradePath {
	return store.GetStore()
}

func UpdateTradeInfo(store TradesActions, trade db.Trade) {
	store.UpdateTradeInfo(trade)
}

func (wt *WaitingTrades) AddTrade(trade db.Trade, safe ...bool) {
	if len(safe) > 0 && safe[0] {
		wt.RLock()
	}
	if _, ok := wt.Store[trade.Symbol]; ok {
		if _, ok := wt.Store[trade.Symbol][trade.UserID]; ok {
			wt.Store[trade.Symbol][trade.UserID][trade.ID] = trade
		} else {
			wt.Store[trade.Symbol][trade.UserID] = make(map[uint]db.Trade)
			wt.Store[trade.Symbol][trade.UserID][trade.ID] = trade
		}
	} else {
		wt.Store[trade.Symbol] = make(map[uint]map[uint]db.Trade)
		wt.Store[trade.Symbol][trade.UserID] = make(map[uint]db.Trade)
		wt.Store[trade.Symbol][trade.UserID][trade.ID] = trade
	}
	if len(safe) > 0 && safe[0] {
		wt.RUnlock()
	}
}

func (wt *WaitingTrades) RemoveTrade(trade db.Trade, safe ...bool) {
	if len(safe) > 0 && safe[0] {
		wt.RLock()
	}
	if _, ok := wt.Store[trade.Symbol]; ok {
		if _, ok := wt.Store[trade.Symbol][trade.UserID]; ok {
			if _, ok := wt.Store[trade.Symbol][trade.UserID][trade.ID]; ok {
				delete(wt.Store[trade.Symbol][trade.UserID], trade.ID)
			}
		}
	}
	if len(safe) > 0 && safe[0] {
		wt.RUnlock()
	}
}

func (wt *WaitingTrades) GetTrade(symbol string, userId uint, tradeId uint, safe ...bool) (db.Trade, error) {
	if len(safe) > 0 && safe[0] {
		wt.RLock()
	}
	if _, ok := wt.Store[symbol]; ok {
		if _, ok := wt.Store[symbol][userId]; ok {
			if _, ok := wt.Store[symbol][userId][tradeId]; ok {
				return wt.Store[symbol][userId][tradeId], nil
			}
		}
	}
	if len(safe) > 0 && safe[0] {
		wt.RUnlock()
	}
	return db.Trade{}, errors.New("not found")
}

func (wt *WaitingTrades) GetStore() *TradePath {
	return &wt.Store
}

func (wt *WaitingTrades) UpdateTradeInfo(trade db.Trade) {
	defer func() {
		if recover() != nil {
			util.LogError(errors.New(fmt.Sprintf("trade index out of bounds in WaitingTrades at:"+
				" [%v][%v][%v]", trade.Symbol, trade.UserID, trade.ID)))
		}
	}()
	wt.RLock()
	wt.Store[trade.Symbol][trade.UserID][trade.ID] = trade
	wt.RUnlock()
}

func (at *ActiveTrades) AddTrade(trade db.Trade, safe ...bool) {
	isSafe := len(safe) > 0 && safe[0]
	if isSafe {
		at.RLock()
	}
	if _, ok := at.Store[trade.Symbol]; ok {
		if _, ok := at.Store[trade.Symbol][trade.UserID]; ok {
			at.Store[trade.Symbol][trade.UserID][trade.ID] = trade
		} else {
			at.Store[trade.Symbol][trade.UserID] = make(map[uint]db.Trade)
			at.Store[trade.Symbol][trade.UserID][trade.ID] = trade
		}
	} else {
		at.Store[trade.Symbol] = make(map[uint]map[uint]db.Trade)
		at.Store[trade.Symbol][trade.UserID] = make(map[uint]db.Trade)
		at.Store[trade.Symbol][trade.UserID][trade.ID] = trade
	}
	if isSafe {
		at.RUnlock()
	}
}

func (at *ActiveTrades) RemoveTrade(trade db.Trade, safe ...bool) {
	isSafe := len(safe) > 0 && safe[0]
	if isSafe {
		at.RLock()
	}
	if _, ok := at.Store[trade.Symbol]; ok {
		if _, ok := at.Store[trade.Symbol][trade.UserID]; ok {
			if _, ok := at.Store[trade.Symbol][trade.UserID][trade.ID]; ok {
				delete(at.Store[trade.Symbol][trade.UserID], trade.ID)
			}
		}
	}
	if isSafe {
		at.RUnlock()
	}
}

func (at *ActiveTrades) GetTrade(symbol string, userId uint, tradeId uint, safe ... bool) (db.Trade, error) {
	if len(safe) > 0 && safe[0] {
		at.RLock()
	}
	if _, ok := at.Store[symbol]; ok {
		if _, ok := at.Store[symbol][userId]; ok {
			if _, ok := at.Store[symbol][userId][tradeId]; ok {
				return at.Store[symbol][userId][tradeId], nil
			}
		}
	}
	if len(safe) > 0 && safe[0] {
		at.RUnlock()
	}
	return db.Trade{}, errors.New("not found")
}

func (at *ActiveTrades) GetStore() *TradePath {
	return &at.Store
}

func (at *ActiveTrades) UpdateTradeInfo(trade db.Trade) {
	defer func() {
		if recover() != nil {
			util.LogError(errors.New(fmt.Sprintf("trade index out of bounds in ActiveTrades at:"+
				" [%v][%v][%v]", trade.Symbol, trade.UserID, trade.ID)))
		}
	}()
	at.RLock()
	at.Store[trade.Symbol][trade.UserID][trade.ID] = trade
	at.RUnlock()
}

func (ct *ClosingTrades) AddTrade(trade db.Trade, safe ...bool) {
	if len(safe) > 0 && safe[0] {
		ct.RLock()
	}
	if _, ok := ct.Store[trade.Symbol]; ok {
		if _, ok := ct.Store[trade.Symbol][trade.UserID]; ok {
			ct.Store[trade.Symbol][trade.UserID][trade.ID] = trade
		} else {
			ct.Store[trade.Symbol][trade.UserID] = make(map[uint]db.Trade)
			ct.Store[trade.Symbol][trade.UserID][trade.ID] = trade
		}
	} else {
		ct.Store[trade.Symbol] = make(map[uint]map[uint]db.Trade)
		ct.Store[trade.Symbol][trade.UserID] = make(map[uint]db.Trade)
		ct.Store[trade.Symbol][trade.UserID][trade.ID] = trade
	}
	if len(safe) > 0 && safe[0] {
		ct.RUnlock()
	}
}

func (ct *ClosingTrades) RemoveTrade(trade db.Trade, safe ...bool) {
	isSafe := len(safe) > 0 && safe[0]
	if isSafe {
		ct.RLock()
	}
	if _, ok := ct.Store[trade.Symbol]; ok {
		if _, ok := ct.Store[trade.Symbol][trade.UserID]; ok {
			if _, ok := ct.Store[trade.Symbol][trade.UserID][trade.ID]; ok {
				delete(ct.Store[trade.Symbol][trade.UserID], trade.ID)
			}
		}
	}
	if isSafe {
		ct.RUnlock()
	}
}

func (ct *ClosingTrades) GetTrade(symbol string, userId uint, tradeId uint, safe ... bool) (db.Trade, error) {
	if len(safe) > 0 && safe[0] {
		ct.RLock()
	}
	if _, ok := ct.Store[symbol]; ok {
		if _, ok := ct.Store[symbol][userId]; ok {
			if _, ok := ct.Store[symbol][userId][tradeId]; ok {
				return ct.Store[symbol][userId][tradeId], nil
			}
		}
	}
	if len(safe) > 0 && safe[0] {
		ct.RUnlock()
	}
	return db.Trade{}, errors.New("not found")
}

func (ct *ClosingTrades) GetStore() *TradePath {
	return &ct.Store
}

func (ct *ClosingTrades) UpdateTradeInfo(trade db.Trade) {
	defer func() {
		if recover() != nil {
			util.LogError(errors.New(fmt.Sprintf("trade index out of bounds in ClosingTrades at:"+
				" [%v][%v][%v]", trade.Symbol, trade.UserID, trade.ID)))
		}
	}()
	ct.RLock()
	ct.Store[trade.Symbol][trade.UserID][trade.ID] = trade
	ct.RUnlock()
}

// end of stores opf trades move

func AddUser(user db.User) {
	users[user.UserId] = user
}

func GetUser(userId uint) (db.User, error) {
	if user, ok := users[userId]; ok {
		return user, nil
	} else {
		return db.User{}, errors.New(fmt.Sprintf("user doesn't exists with user_id: %v", userId))
	}
}
