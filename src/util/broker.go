package util

type Broker struct {
	stopCh      chan struct{}
	publishCh   chan interface{}
	subscribe   chan chan interface{}
	unsubscribe chan chan interface{}
}

func NewBroker() *Broker {
	return &Broker{
		stopCh:      make(chan struct{}),
		publishCh:   make(chan interface{}, 1),
		subscribe:   make(chan chan interface{}, 1),
		unsubscribe: make(chan chan interface{}, 1),
	}
}

func (b *Broker) Start() {
	subs := map[chan interface{}]struct{}{}
	for {
		select {
		case <-b.stopCh:
			return
		case msgCh := <-b.subscribe:
			subs[msgCh] = struct{}{}
		case msgCh := <-b.unsubscribe:
			delete(subs, msgCh)
		case msg := <-b.publishCh:
			for msgCh := range subs {
				// msgCh is buffered, use non-blocking send to protect the broker:
				select {
				case msgCh <- msg:
				}
			}
		}
	}
}

func (b *Broker) Stop() {
	close(b.stopCh)
}

func (b *Broker) Subscribe() chan interface{} {
	msgCh := make(chan interface{}, 5)
	b.subscribe <- msgCh
	return msgCh
}

func (b *Broker) Unsubscribe(msgCh chan interface{}) {
	b.unsubscribe <- msgCh
}

func (b *Broker) Publish(msg interface{}) {
	b.publishCh <- msg
}