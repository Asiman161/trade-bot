FROM golang:1.10.1

COPY . /go/src/go-with-compose
WORKDIR /go/src/go-with-compose
RUN go get -u github.com/golang/dep/cmd/dep
RUN go get -u github.com/codegangsta/gin
RUN go get -u gopkg.in/validator.v2
RUN ["dep", "ensure"]
