package main

import (
	"context"
	"github.com/joho/godotenv"
	"log"
	"time"
	"trade-bot/src/start"
)

func main() {
	envErr := godotenv.Load()
	if envErr != nil {
		log.Fatal("Error loading .env file")
	}
	pgClient := start.ConnectDataBase()
	defer pgClient.Close()

	start.InitUsers()
	start.InitStore()

	exit := make(chan bool)

	// TODO: uncomment it. We will use it when we'll be ready
	//start.InitRedis()
	server := start.InitServer()

	go func() {
		log.Fatal(server.ListenAndServe())
	}()

	go start.ConnectBinanceStream()
	<-exit
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
}
